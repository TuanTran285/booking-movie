export class ThongTinPhongVe {
    thongTinPhim = new ThongTinPhim()
    danhSachGhe = []
}

export class ThongTinPhim {
    maLichChieu = ""
    tenCumRap = ""
    tenRap = ""
    diaChi = ""
    tenPhim = ""
    hinhAnh = " "
    ngayChieu = ""
    gioChieu = ""
}
