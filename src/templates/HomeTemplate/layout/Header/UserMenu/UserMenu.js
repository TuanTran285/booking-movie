import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { NavLink, useNavigate } from 'react-router-dom'
import { TOKEN, USER_LOGIN } from '../../../../../util/settings/config'

export default function UserMenu() {
    const userLogin = useSelector(state => state.QuanLyNguoiDungReducer.userLogin)
    const handleLogut = () => {
        localStorage.removeItem(USER_LOGIN)
        localStorage.removeItem(TOKEN)
        window.location.reload()
    }
    const renderContent = () => {
        if (userLogin) {
            return <div className="items-center flex-shrink-0 hidden lg:flex">
                <div>
                    {userLogin?.hoTen}
                </div>
                <button onClick={handleLogut} className="self-center px-8 py-3 rounded">Logout</button>
            </div>
        } else {
            return <div className="items-center flex-shrink-0 hidden lg:flex">
                <NavLink to="/login"><button className="self-center px-8 py-3 rounded">Sign in</button></NavLink>
                <NavLink to="/register"><button className="self-center px-8 py-3 font-semibold rounded dark:bg-violet-400 dark:text-gray-900">Sign up</button></NavLink>
            </div>
        }
    }
    return (
        renderContent()
    )
}
