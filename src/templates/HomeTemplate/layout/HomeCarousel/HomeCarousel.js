import React, { useEffect } from 'react'
import { Carousel } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { getCloseIcon } from 'antd/es/notification/PurePanel';
import { getCarouselAction } from '../../../../redux/actions/CarouselAction';

const contentStyle = {
    margin: 0,
    // marginTop: 64,
    height: '500px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    backgroundSize: '100%',
    backgroundPositon: 'center',
    backgroundRepeat: 'no-repeat'
};
export default function HomeCarousel() {
    const carouselImg = useSelector(state => state.CarouselReducer.arrImg)
    const dispatch = useDispatch()
    const onChange = (currentSlide) => {
    }

    useEffect(() => {
        dispatch(getCarouselAction())
    }, [])

    const renderImg = () => {
        return carouselImg.map((item, index) => {
            return <div key={index}>
                <div style={{ ...contentStyle, backgroundImage: `url(${item.hinhAnh})` }}>
                    <img src={item.hinhAnh} className="w-full opacity-0" alt="Lỗi" />
                </div>
            </div>
        })
    }
    return (
        <div className='relative'>
            <Carousel className='bg-slate-600' afterChange={onChange}>
                {renderImg()}
            </Carousel>
        </div>
    );
};
