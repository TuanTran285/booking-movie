import { useEffect } from 'react';
import { Fragment } from 'react';
import { Outlet, Route, Router, Routes, useNavigate } from 'react-router-dom';
import Footer from './layout/Footer/Footer';
import Header from './layout/Header/Header';
import HomeCarousel from './layout/HomeCarousel/HomeCarousel';

export function HomeTemplate() {
  useEffect(() => {
    window.scrollTo({top: 0, left: 0, behavior: 'smooth'})
  }, [])
  return (
    <Fragment>
      <Header />
      {/* Outlet chính là cái route component bên tronng route HomeTemplate */}
        <Outlet />
      <Footer />
    </Fragment>

  )
}