import { DesktopOutlined, FileOutlined, PieChartOutlined, TeamOutlined, UserOutlined } from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, theme } from 'antd';
import { useState } from 'react';
import { NavLink, Navigate, Outlet, useNavigate } from 'react-router-dom';
import UserMenu from '../HomeTemplate/layout/Header/UserMenu/UserMenu';
import { useEffect } from 'react';
import { USER_LOGIN } from '../../util/settings/config';
const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}
const items = [
    getItem('Users', '1', <NavLink to='/admin'><UserOutlined /></NavLink>),
    getItem('Films', '2', <DesktopOutlined />, [
      getItem('Films', 'sub1', <NavLink to="/admin/films"><DesktopOutlined /></NavLink>),
      getItem('Add film', 'sub2', <NavLink to="/admin/films/addnew"><FileOutlined /></NavLink>),
      getItem('Edit film', 'sub3', <NavLink to="/admin/films/edit/:id"><FileOutlined /></NavLink>)
    ]),
    getItem('Showtime', '3', <NavLink to="/admin/showtime/id"><PieChartOutlined /></NavLink>),
];
const AdminTemplate = ({component}) => {
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useNavigate()
  useEffect(() => {
    if(JSON.parse(localStorage.getItem(USER_LOGIN))?.maLoaiNguoiDung !== "QuanTri") {
      navigate("/")
    }
  }, [])
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout
      style={{
        minHeight: '100vh',
      }}
    >
      <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
        <div
          style={{
            height: 32,
            margin: 16,
          }}
          className="text-[#edf2f4] font-semibold text-base flex items-center justify-center"
          >Tuấn Trần</div>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        ><div className='flex justify-end'><UserMenu/></div></Header>
        <Content
          style={{
            margin: '0 16px',
          }}
        >
          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: colorBgContainer,
            }}
          >
            {component}
          </div>
        </Content>
        <Footer
          style={{
            textAlign: 'center',
          }}
        >
          Ant Design ©2023 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
};
export default AdminTemplate;