import { useEffect } from 'react';
import { Fragment, } from 'react';
import { Outlet, } from 'react-router-dom';
import { USER_LOGIN } from '../../util/settings/config';
import Footer from '../HomeTemplate/layout/Footer/Footer';
import Header from '../HomeTemplate/layout/Header/Header';

export function CheckoutTemplate() {
  useEffect(() => {
    window.scrollTo({top: 0, left: 0, behavior: 'smooth'})
  }, [])
  if (!localStorage.getItem(USER_LOGIN)) {
    return window.location.href = "/login"
  }
  return (
    <Fragment>
      <Outlet />
    </Fragment>
  )
}