import AdminAddFilm from "../pages/Admin/AdminAddFilm/AdminAddFilm";
import AdminMoviePage from "../pages/Admin/AdminFilmPage/AdminFilmPage";
import AdminShowTime from "../pages/Admin/AdminShowTime/AdminShowTime";
import AdminUserPage from "../pages/Admin/AdminUserPage/AdminUserPage";
import Dashboard from "../pages/Admin/Dashboard/Dashboard";
import EditFilm from "../pages/Admin/EditFilm/EditFilm";

export const adminRoutes = [
    {
        url: '/admin',
        component: <AdminUserPage/>
    },
    {
        url: '/admin/user',
        component: <AdminUserPage/>

    },
    {
        url: '/admin/films',
        component: <AdminMoviePage/>
    },
    {
        url: '/admin/films/addnew',
        component: <AdminAddFilm/>
    },
    {
        url: '/admin/films/edit/:id',
        component: <EditFilm />
    },
    {
        url: '/admin/showtime/:id',
        component: <AdminShowTime />
    },
]