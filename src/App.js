import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import Checkout from "./pages/Checkout/Checkout";
import Contact from "./pages/Contact/Contact";
import Detail from "./pages/Detail/Detail";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import New from "./pages/New/New";
import Register from "./pages/Register/Register";
import { CheckoutTemplate } from "./templates/CheckoutTemplate/CheckoutTemplate";
import { HomeTemplate } from "./templates/HomeTemplate/HomeTemplate";
import { UserTemplate } from "./templates/UserTemplate/UserTemplate";
import Loading from "./components/Loading/Loading";
import { adminRoutes } from "./routes/adminRoutes";
import Dashboard from "./pages/Admin/Dashboard/Dashboard";
import AdminTemplate from "./templates/AdminTemplate/AdminTemplate";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Loading />
        <Routes>
           <Route path="" element={<HomeTemplate />}>
            <Route path="/" element={<Home />}></Route>
            <Route path="/home" element={<Home />}></Route>
            <Route path="/contact" element={<Contact />}></Route>
            <Route path="/new" element={<New />}></Route>
            <Route path="/detail/:id" element={<Detail />}></Route>
          </Route>
          <Route path="" element={<CheckoutTemplate />}>
            <Route path="/checkout/:id" element={<Checkout />} />
          </Route>
          <Route path="" element={<UserTemplate />}>
            <Route path="/login" element={<Login />}></Route>
            <Route path="/register" element={<Register />}>
            </Route>
          </Route>
          {adminRoutes.map(({url, component}) => {
            return <Route key={url} path={url} element={<AdminTemplate component={component}/>}/>
          })}
        </Routes>
        <ToastContainer />
      </BrowserRouter>
    </div>
  )
}
export default App;
