// util định nghĩa những tham số cứng cố định

export const DOMAIN = 'https://movienew.cybersoft.edu.vn'
export const USER_LOGIN = 'USER_LOGIN' 
export const TokenCybersoft = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjEwLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDMwNDAwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0NDUxNjAwfQ.sBqNvFEzAEqAZuxinnH_gzedfmLxPTf7WONjIlV-Q7U' 
export const TOKEN = 'accessToken'

export const configHeaders =() => {
    return {
        TokenCybersoft: TokenCybersoft,
        Authorization: "bearer "+localStorage.getItem(TOKEN)
    }
}

