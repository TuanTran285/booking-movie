import { Component } from "react"
import  https  from "./baseService"
export class QuanLyPhimService extends Component {
    constructor() {
        super()
    }

    layDanhSachBanner = () => {
        return https.get('/api/QuanLyPhim/LayDanhSachBanner')
    }
    layDanhSachPhim = (tuKhoa) => {
        if(!tuKhoa) {
            return https.get('/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP11')
        }else {
            return https.get(`https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP11&tenPhim=${tuKhoa}`)
        }
    }
    themPhimUploadHinh = (formData) => {
        return https.post('/api/QuanLyPhim/ThemPhimUploadHinh', formData)
    }
    layThongTinPhim = (maPhim) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`)
    }
    capNhatPhimUpload = (formData) => {
        return https.post('/api/QuanLyPhim/CapNhatPhimUpload', formData)
    }
    xoaPhim = (maPhim) => {
        return https.delete(`/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`)
    }
}

export const quanLyPhimService = new QuanLyPhimService()