import { Component } from "react"
import https  from "./baseService"
export class QuanLyRapService extends Component {
    constructor() {
        super()
    }

    layHeThongRap = (GROUPID) => {
        return https.get(`/api/QuanLyRap/LayThongTinLichChieuHeThongRap`)
    }
    layChiTietThongTinPhim = (id) => {
        return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`)
    }
    layThongTinHeThongRap = () => {
        return https.get('/api/QuanLyRap/LayThongTinHeThongRap')
    }
    layThongTinCumRap = (maHeThongRap) => {
        return https.get(`api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${maHeThongRap}`)
    }
}

export const quanLyRapService = new QuanLyRapService()