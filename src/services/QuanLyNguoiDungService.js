import { Component } from "react"
import  https  from "./baseService"
class QuanLyNguoiDungService extends Component {
    constructor() {
        super()
    }

    dangNhap = (thongTinDangNhap) => {
        return https.post("/api/QuanLyNguoiDung/DangNhap", thongTinDangNhap)
    }
    
    layThongTinNguoiDung = () => {
        return https.post("/api/QuanLyNguoiDung/ThongTinTaiKhoan")
    }
    layDanhSachNguoiDung = (tuKhoa) => {
        if(!tuKhoa) {
            return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP00")
        }else {
            return https.get(`/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP00&tuKhoa=${tuKhoa}`)
        }
    }
    xoaNguoiDung = (taiKhoan) => {
        return https.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`)
    }
}

export const quanLyNguoiDungService = new QuanLyNguoiDungService()