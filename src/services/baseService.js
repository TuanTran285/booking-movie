import axios from "axios";
import { configHeaders, DOMAIN } from "../util/settings/config"


export default  axios.create({
    baseURL: DOMAIN,
    headers: configHeaders()
});