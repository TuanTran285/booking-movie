import { Component } from "react"
import  https  from "./baseService"
export class QuanLyDatVeService extends Component {
    constructor() {
        super()
    }

    layChiTietPhongVe = (maLichChieu) => {
        return https.get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`)
    }

    datVeService = (thongTinDatVe) => {
        return https.post(`/api/QuanLyDatVe/DatVe`, thongTinDatVe)
    }
}

export const quanLyDatVeService = new QuanLyDatVeService()