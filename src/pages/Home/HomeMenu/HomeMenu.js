import React, { memo } from 'react';
import { Tabs } from 'antd';
import ItemMenuMovie from './ItemMenuMovie';

const onChange = (key) => {
  console.log(key);
}

function HomeMenu({ heThongRapChieu }) {

  const renderHeThongRap = () => {
    return heThongRapChieu.map((heThongRap, index) => {
      return {
        key: index,
        label: <img className='h-14' src={heThongRap.logo} alt="Lỗi" />,
        children: (
          <Tabs className='h-[600px]' tabPosition="left" defaultActiveKey="1" items={heThongRap.lstCumRap.map((rap, index) => {
            return {
              key: index,
              label: <div className='max-w-[250px] text-left'>
                <h3 className='text-green-500 font-medium text-base'>{rap.tenCumRap}</h3>
                <p>{rap.diaChi.length > 30 ? rap.diaChi.slice(0, 30) + '...' : rap.diaChi}</p>
              </div>,
              children: <div className='h-[600px] overflow-y-scroll'>
                <ItemMenuMovie rap={rap} key={index} />
              </div>
            }
          })} onChange={onChange}>
          </Tabs>
        )
      }
    })
  }
  return (
    <div className='container'>
      <Tabs tabPosition="left" defaultActiveKey="1" items={renderHeThongRap()} onChange={onChange}>
      </Tabs>
    </div>
  )
}

export default memo(HomeMenu)
