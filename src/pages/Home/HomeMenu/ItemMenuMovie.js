import React from 'react'
import moment from 'moment';
import { NavLink } from 'react-router-dom';

export default function ItemMenuMovie({ rap }) {
  return (
    <div>
      {rap.danhSachPhim.map((phim, index) => {
        return <div key={index} className='my-8 flex '>
          <img className='w-32 h-32 object-cover rounded-lg' src={phim.hinhAnh} alt="" />
          <div className='flex flex-col ml-4'>
            <h3 className='font-semibold text-lg'>{phim.tenPhim}</h3>
            {/* bọc thẻ div vào cho nó đỡ lấy kích thước bằng thẻ cha */}
            <div>
              {phim.lstLichChieuTheoPhim.map((time, index) => {
                return <NavLink key={index} to={`/checkout/${time.maLichChieu}`}>
                  <button
                    className='border-[1px] border-gray-900
                hover:scale-110 transition-all duration-200 bg-white shadow-md p-2 mr-4 mb-2 rounded-lg text-green-500 font-bold'>
                    {moment(time.ngayChieuGioChieu).format('DD-MM-YYYY ~ hh:mm')}
                  </button>
                </NavLink>
              })}
            </div>
          </div>
        </div>
      })}
    </div>
  )
}
