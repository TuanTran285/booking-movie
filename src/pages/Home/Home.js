import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ToastContainer } from 'react-toastify'
import Film from '../../components/Film/Film'
import MultipleRowSlick from '../../components/MultipleRowSlick/MultipleRowSlick'
import { layDanhSachPhimAction } from '../../redux/actions/QuanLyPhimAction'
import { layThongTinHeThongRapAction } from '../../redux/actions/QuanLyRapAction'
import HomeCarousel from '../../templates/HomeTemplate/layout/HomeCarousel/HomeCarousel'
import HomeMenu from './HomeMenu/HomeMenu'

export default function Home() {
  const { arrFilm } = useSelector(state => state.QuanLyPhimReducer)
  const { heThongRapChieu } = useSelector(state => state.QuanLyRapReducer)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(layDanhSachPhimAction())
    dispatch(layThongTinHeThongRapAction())
  }, [])
  return (
    <div>
      <HomeCarousel />
      <div className='my-30'>
        <section className="text-gray-600 body-font">
          <div className="container px-5 py-24 mx-auto">
            <MultipleRowSlick arrFilm={arrFilm} />
          </div>
        </section>
        {/* sử dụng memo cho thằng homeMenu là vì khi dispacth thắng lấy danh sách phim thì thằng homeMenu không thay đổi nên k render lại */}
        {/* khi dispatch lấy thông tin hệ thống rạp có sự thay đổi thì thằng heThongRapChieu mới thay đổi */}
        <div className="mb-24" >
          <HomeMenu heThongRapChieu={heThongRapChieu} />
        </div>
      </div>
      
      
    </div>
  )
}

