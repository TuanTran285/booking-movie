import { Progress, Rate, Space } from 'antd'
import React, { useEffect } from 'react'
import { Tabs } from 'antd';
import '../../assets/styles/circle.css'
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import { layChiTietThongTinPhimAction } from '../../redux/actions/QuanLyRapAction';
import moment from 'moment/moment';


export default function () {
  const { id } = useParams()
  const { detailFilm } = useSelector(state => state.QuanLyRapReducer)
  const dispatch = useDispatch()
  const onChange = (key) => {
  }
  const renderData = [
    {
      key: '1',
      label: `Lịch chiếu`,
      children: (
        // dùng toán tử oparating nếu có thì sẽ render ngược lại không có sẽ không render
        <Tabs className='text-lg' tabPosition="left" items={
          detailFilm.heThongRapChieu?.map((heThongRap, index) => {
            return {
              key: index,
              label: <img src={heThongRap.logo} className="w-14 h-14" alt="Lỗi" />,
              children: heThongRap.cumRapChieu?.map((rap, index) => {
                return <div key={index} className="mb-4">
                  <div className='flex'>
                    <img className='w-14 h-14 object-cover rounded-md' src={rap.hinhAnh} alt="Lỗi" />
                    <h4 className='ml-2'>{rap.tenCumRap}</h4>
                  </div>
                  <div className='flex flex-wrap'>
                    {rap.lichChieuPhim?.slice(0, 10).map((lichChieu, index) => {
                      return <div className='m-4' key={index}>
                      <NavLink to={`/checkout/${lichChieu.maLichChieu}`}>
                        <button className='bg-slate-500 rounded-md text-white p-2'>
                          {moment(lichChieu.ngayChieuGioChieu).format('DD-MM-YYYY ~ HH:mm')}
                        </button>
                      </NavLink>
                      </div>
                    })}
                  </div>
                </div>
              })
            }
          })
        } defaultActiveKey="1" onChange={onChange} />
      ),
    },
    {
      key: '2',
      label: `Thông tin`,
      children: `Content of Tab Pane 2`,
    },
    {
      key: '3',
      label: `Đánh giá`,
      children: `Content of Tab Pane 3`,
    },
  ];

  useEffect(() => {
    dispatch(layChiTietThongTinPhimAction(id))
    window.scrollTo({top: 0, left: 0, behavior: 'smooth'})
  }, [])

  return (
    <div className='min-h-screen -z-10 text-white bg-no-repeat bg-cover bg-[100%]'
      style={{ backgroundImage: `url(${detailFilm.hinhAnh})` }}>
      <div className='h-full w-full backdrop-blur-sm bg-black/40'>
        <div className='container px-20 py-20 text-white'>
          <div className='my-10 grid grid-cols-12'>
            <div className='col-span-8 flex items-center text-white'>
              <img className='w-[240px] h-[350px] object-cover rounded-lg'
                src={detailFilm.hinhAnh} alt="" />
              <div className='ml-5'>
                <h4 className='font-semibold text-xl'>Lịch chiếu: {detailFilm.ngayKhoiChieu}</h4>
                <h3 className='font-bold'>Phim: {detailFilm.tenPhim}</h3>
                <p className='font-medium'>Về phim: {detailFilm.moTa}</p>
              </div>
            </div>
            <Space className='col-span-4 flex flex-col items-center ml-auto' wrap>
              <Rate className='items-center mb-5 text-green-400' allowHalf style={{}} defaultValue={detailFilm.danhGia / 2} />
              <Progress type='circle' size={200} percent={detailFilm.danhGia * 10} strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }} status='active' />
            </Space>
          </div>
          <div className='bg-white text-black h-auto rounded-md p-4'>
            <Tabs className='text-lg' tabPosition="top" defaultActiveKey="1" items={renderData} onChange={onChange} />
          </div>
        </div>
      </div>
    </div>
  )
}
