import React, { Fragment } from 'react'
import { USER_LOGIN } from '../../util/settings/config'
import './Checkout.css'
import { DAT_VE } from '../../redux/actions/Type/QuanLyDatVeType'
import { Tabs } from 'antd'
import CheckoutSeats from './CheckoutSeats/CheckoutSeats'
import CheckoutHistory from './CheckoutHistory/CheckoutHistory'
import { useState } from 'react'
import { NavLink } from 'react-router-dom'
import UserMenu from '../../templates/HomeTemplate/layout/Header/UserMenu/UserMenu'
export default function Checkout() {
  const [activeKey, setActiveKey] = useState('1')

  const handleChangeTab =() => {
    setActiveKey('2')
  }
  const onChange = (key) => {
    setActiveKey(key)
  }
  const items = [
    {
      key: '',
      label: <NavLink to="/" className="mx-4 text-xl font-bold">Home</NavLink>
    },
    {
      key: '1',
      label: <div className='mx-4'>CHỌN GHẾ VÀ THANH TOÁN</div>,
      children: <CheckoutSeats handleChangeTab={handleChangeTab} />
    },
    {
      key: '2',
      label: <div>KẾT QUẢ ĐẶT VÉ</div>,
      children: <CheckoutHistory />
    },
  ]
  return (
    <div className='flex flex-col'>
      <Tabs activeKey={activeKey} items={items} onChange={onChange} />
    </div>

  )
}



