import React, { Fragment } from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { datVeAction, layThongTinPhongVeAction } from '../../../redux/actions/QuanLyDatVeAction'
import { CheckOutlined, CloseOutlined, UserOutlined } from '@ant-design/icons'
import { DAT_VE } from '../../../redux/actions/Type/QuanLyDatVeType'
import { memo } from 'react'
import { loadingPlayAction } from '../../../redux/actions/LoadingAction'

function CheckoutSeats({handleChangeTab}) {
    const { id } = useParams()
    const dispatch = useDispatch()
    const { userLogin } = useSelector(state => state.QuanLyNguoiDungReducer)
    const { chiTietPhongVe, DanhSachgheDangDat, danhSachGheKhachDat} = useSelector(state => state.QuanLyDatVeReducer)
    const { thongTinPhim, danhSachGhe } = chiTietPhongVe
    console.log(DanhSachgheDangDat)
    useEffect(() => {
        dispatch(layThongTinPhongVeAction(id))
    }, [])
    // console.log(danhSachGheKhachDat)
    const renderDanhSachGhe = () => {
        return danhSachGhe.map((ghe, index) => {
            let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : ""
            let classGheDaDat = ghe.daDat ? "gheDaDat" : ""
            let indexGheDangDat = DanhSachgheDangDat.findIndex(gheDangDat => {
                return gheDangDat.maGhe === ghe.maGhe
            })
            let classGheDangDat = indexGheDangDat !== -1 ? "gheDangDat" : ""
            let classGheDaDuocDat = ""
            if (userLogin.taiKhoan === ghe.taiKhoanNguoiDat) {
                classGheDaDat = 'gheDaDuocDat'
            }
            let classsGheKhachDat = ''
            let indexGheKhachDat = danhSachGheKhachDat.findIndex(gheKD => {
                return gheKD.maGhe === ghe.maGhe
            })
            if(indexGheKhachDat !== -1) {
                classsGheKhachDat = 'gheKhachDat'
            }

            return <Fragment key={index}>
                <button disabled={ghe.daDat || classsGheKhachDat !== ''} onClick={() =>
                    dispatch({
                        type: DAT_VE,
                        ghe
                    })}
                    className={`ghe ${classGheVip} ${classGheDaDat} ${classGheDangDat} ${classGheDaDuocDat} ${classsGheKhachDat}`} key={index}>
                    {ghe.daDat ? classGheDaDat !== '' ? <UserOutlined /> : <CloseOutlined /> : ghe.stt}
                </button>
                {/* làm cho 16 ghế trên 1 dòng */}
                {(index + 1) % 16 === 0 ? <br /> : ''}
            </Fragment>

        }
        )
    }
    const totalMoney = DanhSachgheDangDat.reduce((tongTien, ghe) => {
        return tongTien += ghe.giaVe
    }, 0)

    return (
        <div>
            <div className={`grid grid-cols-12 overflow-hidden py-4 text-white relative`}>
                <div className='h-full w-full blur-xl bg-no-repeat bg-cover bg-left-top -z-10 absolute' style={{ backgroundImage: `url(${thongTinPhim.hinhAnh})` }}></div>
                <div className='col-span-9 container pt-4 flex flex-col items-center'>
                    <div className='h-4 bg-black w-full'></div>
                    <div className='trapezoid text-white font-semibold text-center'>
                        Màn hình
                    </div>
                    <div className=''>
                        {renderDanhSachGhe()}
                        <div className='mt-10 flex justify-center'>
                            <table className='table divide-y divide-gray-200 w2/3'>
                                <thead>
                                    <tr>
                                        <th>Ghế chưa đặt</th>
                                        <th>Ghế đang đặt</th>
                                        <th>Ghế víp</th>
                                        <th>Ghế đã đặt</th>
                                        <th>Ghế mình đặt</th>
                                        <th>Ghế khách đặt</th>
                                    </tr>
                                </thead>
                                <tbody className='divide-y divide-gray-200'>
                                    <tr>
                                        <td><button className="ghe"><CheckOutlined /></button></td>
                                        <td><button className="ghe gheDangDat"><CheckOutlined /></button></td>
                                        <td><button className="ghe gheVip"><CheckOutlined /></button></td>
                                        <td><button className="ghe gheDaDat"><CheckOutlined /></button></td>
                                        <td> <button className="ghe gheDaDuocDat"><CheckOutlined /></button></td>
                                        <td> <button className="ghe gheKhachDat"><CheckOutlined /></button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className='col-span-3 shadow-md'>
                    <div className='p-4 border-b-[1px] border-gray-300 text-cyan-300 text-center'>Tổng tiền: {totalMoney.toLocaleString()} VND</div>
                    <div className='p-4 border-b-[1px] border-gray-300'>
                        <h4>Tên phim: {thongTinPhim?.tenPhim}</h4>
                        <h5>Địa điểm: {thongTinPhim?.diaChi}</h5>
                        <p>Ngày chiếu: {thongTinPhim?.ngayChieu}</p>
                    </div>
                    <div className='p-4 border-b-[1px] border-gray-300 justify-between flex flex-col'>
                        <h4><span>Ghế: </span> {DanhSachgheDangDat.sort((a, b) => a.stt - b.stt).map(ghe => {
                            return <span className='text-cyan-500a' key={ghe.stt}>{ghe.stt} </span>
                        })}</h4>
                        <span className='text-cyan-300'>Tổng tiền: {totalMoney.toLocaleString()} VND</span>
                    </div>
                    <div className='p-4 border-b-[1px] border-gray-300'>Email: {userLogin.email}</div>
                    <div className='p-4 border-b-[1px] border-gray-300'>Phone: {userLogin.soDT}</div>
                    <div className=''>
                        <button onClick={() => {
                            dispatch(datVeAction({
                                maLichChieu: id,
                                danhSachVe: DanhSachgheDangDat
                            }, handleChangeTab))
                            dispatch(loadingPlayAction)
                        }} className='w-full block p-4  my-4 bg-[#57cc99] text-white rounded-md'>Đặt vé</button>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default memo(CheckoutSeats)