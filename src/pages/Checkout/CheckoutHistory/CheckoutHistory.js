import React from 'react'
import { useEffect } from 'react'
import { memo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { layThongTinNguoiDungAction } from '../../../redux/actions/QuanLyNguoiDungAction'
import moment from 'moment/moment'
import { reverse } from 'lodash'

export default function CheckoutHistory() {
  const dispatch = useDispatch()
  const { thongTinNguoiDung } = useSelector(state => state.QuanLyNguoiDungReducer)
  console.log(thongTinNguoiDung)
  useEffect(() => {
    dispatch(layThongTinNguoiDungAction())
  }, [])
  const renderTicketItem = () => {
    return thongTinNguoiDung.thongTinDatVe?.reverse().map((ticket) => {
      return ticket.danhSachGhe.reverse().map((ghe, index) => {
        return <tr key={index} className="border-b border-opacity-20 text-base dark:border-gray-700 dark:bg-gray-900">
          <td className="p-3">
            <p>
              <span className='font-semibold text-sm block mb-2'>{ticket.tenPhim}</span>
              <img className='w-20 h-20 mx-auto rounded-md object-cover' src={ticket.hinhAnh} alt="" />
            </p>
          </td>
          <td className="p-3">
            <p>{moment(ticket.ngayDat).format('MM-DD-YYYY hh:mm:ss')}</p>
          </td>
          <td className="p-3">
            <p>{ghe.tenHeThongRap}</p>
          </td>
          <td className="p-3">
            <p>{ghe.tenRap} Ghế: {ghe.tenGhe}</p>
          </td>
          <td className="p-3 text-right">
            <p>{ticket.giaVe.toLocaleString()} VND</p>
          </td>
          <td className="p-3 text-right">
            <span className="px-3 py-1 font-semibold rounded-md dark:bg-violet-400 dark:text-gray-900">
              <button className='p-2 bg-green-600 rounded-md text-white'>Success</button>
            </span>
          </td>
        </tr>
      })
    })
  }
  return (
    <div className="container p-2 mx-auto sm:p-4 dark:text-gray-100">
      <h2 className="mb-4 text-2xl font-semibold leading-tight">Invoices</h2>
      <div className="overflow-x-auto">
        <table className="min-w-full text-xs text-center">
          <thead className="dark:bg-gray-700 text-lg">
            <tr className="text-left">
              <th className="p-3">Tên phim</th>
              <th className="p-3">Giờ chiếu</th>
              <th className="p-3">Địa điểm</th>
              <th className="p-3">Tên rạp</th>
              <th className="p-3 text-right">Giá vé</th>
              <th className="mr-0 text-right">Trạng thái</th>
            </tr>
          </thead>
          <tbody>
            {renderTicketItem()}
          </tbody>
        </table>
      </div>
    </div>
  )
}
