import { Button, Space, Table, Tag } from 'antd';
import React from 'react'
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { layDanhSachNguoiDungAction, timKiemNguoiDungAction, xoaNguoiDungAction } from '../../../redux/actions/QuanLyNguoiDungAction';
import './AdminUserPage.css'
import { DeleteOutlined, ExclamationCircleFilled } from '@ant-design/icons'
import { Modal } from 'antd';
import Search from 'antd/es/transfer/search';
const { confirm } = Modal;

export default function AdminUserPage() {
  let { danhSachNguoiDung } = useSelector(state => state.QuanLyNguoiDungReducer)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(layDanhSachNguoiDungAction())
  }, [])
  const handleDeleteUser = (taiKhoan) => {
    confirm({
      title: `Bạn có chắc muốn xóa tài khoản ${taiKhoan} này không?`,
      icon: <ExclamationCircleFilled />,
      okText: 'Yes',
      cancelText: 'No',
      async onOk() {
        try{
          await dispatch(xoaNguoiDungAction(taiKhoan))
          await dispatch(layDanhSachNguoiDungAction())
        }catch(err) {
          console.log("err", err)
        }
      },
      onCancel() {
        
      },
    })
  }
  const timKiemNguoiDung = (e) => {
    let {value} = e.target
    dispatch(layDanhSachNguoiDungAction(value))
  }
  const headerUsers = [
    {
      title: 'Tài khoản',
      dataIndex: 'taiKhoan',
      key: 'taiKhoan',
    },
    {
      title: 'Họ tên',
      dataIndex: 'hoTen',
      key: 'hoTen',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Mật khẩu',
      dataIndex: 'matKhau',
      key: 'matKhau',
    },
    {
      title: 'Số DT',
      dataIndex: 'soDT',
      key: 'soDT',
    },
    {
      title: 'Loại người dùng',
      dataIndex: 'maLoaiNguoiDung',
      key: 'maLoaiNguoiDung',
      render: (text, user) => {
        if(text === "KhachHang") {
          return <Tag color="blue">Khách hàng</Tag>
        }else {
          return <Tag color="red">Quản trị</Tag>
        }
      }
    },
    {
      title: 'Thao tác',
      key: 'thaoTac',
      render: (text, user) => {
        return <Button onClick={() => handleDeleteUser(user.taiKhoan)} className='flex items-center' type='primary' danger><DeleteOutlined /></Button>
      }
    },
  ];

  return (
    <div>
      <h2 className='text-blue-500 font-bold text-lg'>Quản lý người dùng</h2>
      <Search
      placeholder="input search text"
      allowClear
      enterButton="Search"
      onChange={timKiemNguoiDung}
    />
      <Table className='text-left' columns={headerUsers} dataSource={danhSachNguoiDung} />
    </div>
  )
}
