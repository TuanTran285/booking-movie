import React, { useState } from 'react'
import { Button, Cascader, DatePicker, Form } from 'antd';
import { useEffect } from 'react';
import { quanLyRapService } from '../../../services/QuanLyRapService';
import './AdminShowTime.css'

export default function AdminShowTime() {
  const [state, setState] = useState({
    heThongRapChieu: [],
    cumRapChieu: [],
  })

  useEffect(() => {
    const handleApi = async () => {
      try {
        const result = await quanLyRapService.layThongTinHeThongRap()
        setState({
          ...state,
          heThongRapChieu: result.data.content
        })
      } catch (err) {
        console.log("err", err)
      }

    }
    handleApi()
  }, [])



  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const hanleChangeHeThongRap = async (value) => {
    console.log(value)
    try {
      const result = await quanLyRapService.layThongTinCumRap(value)
      setState({
        ...state,
        cumRapChieu: result.data.content
      })
    } catch (err) {
      console.log("err", err)
    }
  };


  const handleChangeCumRap = (value) => {
    console.log(value)
  }

  const onOk = (value) => {
    console.log('onOk: ', value);
  };
  const onChange = (value, dateString) => {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
  };

  return (
    <div className='container'>
      <h2 className='text-2xl'>Tạo Lịch Chiếu</h2>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Hệ thống rạp"
          name="heThongRap"
          rules={[
            {
              required: true,
              message: 'Vui lòng chọn hệ thông rạp!',
            },
          ]}
        >
          <Cascader options={state.heThongRapChieu.map((htr, index) => {
            return { label: htr.tenHeThongRap, value: htr.maHeThongRap }
          })} onChange={hanleChangeHeThongRap} placeholder="Please select" />
        </Form.Item>

        <Form.Item
          label="Cụm rạp"
          name="cumRap"
          rules={[
            {
              required: true,
              message: 'Vui lòng chọn hệ cụm rạp!',
            },
          ]}
        >
          <Cascader options={state.cumRapChieu.map((cumRap, index) => {
            return { label: cumRap.tenCumRap, value: cumRap.tenCumRap }
          })} onChange={handleChangeCumRap} placeholder="Please select" />
        </Form.Item>

        <Form.Item
          label="Ngày chiếu lịch chiếu"
          // name="ngayChieu"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <DatePicker showTime onChange={onChange} onOk={onOk} />
        </Form.Item>


        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}
