import { PlusOutlined } from '@ant-design/icons';
import {
    Button, Cascader, DatePicker, Form, Input, InputNumber, message, Radio, Select, Switch, TreeSelect, Upload,
} from 'antd';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { capNhatPhimUploadAction, layThongTinPhimAction } from '../../../redux/actions/QuanLyPhimAction';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import moment from 'moment/moment';
const EditFilm = () => {
    const [componentSize, setComponentSize] = useState('default');
    const [imgSrc, setImgSrc] = useState({})
    const [file, setFile] = useState(null)
    const dispatch = useDispatch()
    const { thongTinPhim } = useSelector(state => state.QuanLyPhimReducer)
    const { id } = useParams()
    useEffect(() => {
        dispatch(layThongTinPhimAction(id))
    }, [])
    console.log(thongTinPhim)
    const onFormLayoutChange = ({ size }) => {
        setComponentSize(size);
    };
    const onFinish = (values) => {
        let ngayKhoiChieu = values.ngayKhoiChieu.format('DD/MM/YYYY')
        const newValues = { ...values, maNhom: 'GP11', hinhAnh: file, ngayKhoiChieu: ngayKhoiChieu}
        const formData = new FormData()
        for (let key in newValues) {
            if (key !== 'hinhAnh') {
                formData.append(key, newValues[key])
            } else {
                if(newValues.hinhAnh !== null) {
                    formData.append('File', newValues.hinhAnh, newValues.hinhAnh.name)
                }
            }
        }
        console.log(formData)
        dispatch(capNhatPhimUploadAction(formData))
    };
    const validateMessages = {
        required: '${label} is required!',
        types: {
            email: '${label} is not a valid email!',
            number: '${label} is not a valid number!',
        },
        number: {
            range: '${label} must be between ${min} and ${max}',
        },
    };

    const handleChangeFile = (e) => {
        // lấy ra file từ e
        let file = e.target.files[0]
        setFile(file)
        // tạo đối tượng để đọc file
        let reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = (e) => {
            setImgSrc(e.target.result)
        }
    }
    return (
        <div>
            <h2 className='text-lg'>Thêm phim mới</h2>
            <Form onFinish={onFinish}
                key={thongTinPhim?.maPhim}
                validateMessages={validateMessages}
                className='containner mx-auto'
                labelCol={{
                    span: 4,
                }}
                wrapperCol={{
                    span: 14,
                }}
                layout="horizontal"
                shou
                initialValues={{
                    size: componentSize,
                    tenPhim: thongTinPhim?.tenPhim,
                    trailer: thongTinPhim?.trailer,
                    moTa: thongTinPhim?.moTa,
                    ngayKhoiChieu: moment(thongTinPhim?.ngayKhoiChieu),
                    dangChieu: thongTinPhim?.dangChieu,
                    sapChieu: thongTinPhim?.sapChieu,
                    hot: thongTinPhim?.hot,
                    soSao: thongTinPhim?.danhGia,
                    hinhAnh: null,
                }}
                onValuesChange={onFormLayoutChange}
                size={componentSize}
                style={{
                    maxWidth: 600,
                }}
            >
                <Form.Item label="Form Size" name="size" >
                    <Radio.Group>
                        <Radio.Button value="small">Small</Radio.Button>
                        <Radio.Button value="default">Default</Radio.Button>
                        <Radio.Button value="large">Large</Radio.Button>
                    </Radio.Group>
                </Form.Item>
                <Form.Item label="Tên phim" name='tenPhim' rules={[{ required: true }]} shouldUpdate={(prevValues, curValues) => prevValues.additional !== curValues.additional}>
                    <Input />
                </Form.Item>
                <Form.Item label="Trailer" name='trailer' rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Mô tả" name='moTa' rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Ngày chiếu" name='ngayKhoiChieu'>
                    <DatePicker format={'DD/MM/YY'} />
                </Form.Item>
                <Form.Item label="Đang chiếu" valuePropName="checked" initialValue={false} name='dangChieu'>
                    <Switch />
                </Form.Item>
                <Form.Item label="Sắp chiếu" valuePropName="checked" initialValue={false} name='sapChieu'>
                    <Switch />
                </Form.Item>
                <Form.Item label="Hot" valuePropName="checked" initialValue={false} name='hot'>
                    <Switch />
                </Form.Item>
                <Form.Item label="Số sao" name='soSao' rules={[{ required: true, type: 'number', min: 1, max: 10 }]}>
                    <InputNumber />
                </Form.Item>
                <Form.Item label="Thêm hình"
                    rules={[{ required: true, message: 'Vui lòng chọn hình ảnh!' }]}>
                    <input type="file" onChange={handleChangeFile} />
                    <img src={imgSrc} className="w-[120px] h-[120px] rounded-md mt-3" alt="" />
                </Form.Item>
                <Form.Item label="Cập nhật">
                    <Button htmlType='submit' type="primary">Cập nhật</Button>
                </Form.Item>
            </Form>
        </div>
    );
};
export default EditFilm
