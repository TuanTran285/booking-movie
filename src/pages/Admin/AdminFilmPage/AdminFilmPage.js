import { EditOutlined, DeleteOutlined, ExclamationCircleFilled, PieChartOutlined } from '@ant-design/icons'
import { Button, Table } from 'antd'
import React, { Fragment, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import InputSearch from '../../../components/InputSearch/InputSearch'
import { layDanhSachPhimAction, xoaPhimAction } from '../../../redux/actions/QuanLyPhimAction'
import { Modal } from 'antd';
import Search from 'antd/es/transfer/search'
const { confirm } = Modal;

export default function AdminFilmPage() {
  const {arrFilm} = useSelector(state => state.QuanLyPhimReducer)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(layDanhSachPhimAction())
  }, [])
  const handleDeleteFilm = (film) => {
    confirm({
      title: <div>Bạn có chắc muốn xóa phim <span className='text-red-500'>{film.tenPhim}</span> này không?</div>,
      icon: <ExclamationCircleFilled />,
      okText: 'Yes',
      cancelText: 'No',
      async onOk() {
        await dispatch(xoaPhimAction(film.maPhim))
        await dispatch(layDanhSachPhimAction())
      },
      onCancel() {
        
      },
    })
  }

  const handleSerachOnChange = (e) => {
    let {value} = e.target
    dispatch(layDanhSachPhimAction(value))
  }
  
const headersFilm = [
  {
    title: 'Mã phim',
    dataIndex: 'maPhim',
    key: 'maPhim',
    sorter:(a, b) => a.maPhim - b.maPhim,
    defaultSortOrder: 'descend',
    width: 150
  },
  {
    title: 'Hình ảnh',
    dataIndex: 'hinhAnh',
    key: 'hinhAnh',
    render: (text, films) => {
      return <img className='w-16 h-16' src={text} alt="" />
    },
    width: 150
  },
  {
    title: 'Tên phim',
    dataIndex: 'tenPhim',
    key: 'tenPhim',
    width: 300
  },
  {
    title: 'Mô tả',
    dataIndex: 'moTa',
    key: 'moTa',
    render: (text, film) => {
      return <Fragment>{film.moTa.length > 50 ? film.moTa.slice(0, 50) + '...': film.moTa}</Fragment>
    },
    sortDirections: ['descend',],
  },
  {
    title: 'Thao tác',
    key: 'thaoTac',
    render: (text, film) => {
      return <div className='flex'>
      <NavLink key={1} to={`/admin/films/edit/${film.maPhim}`}><Button type='primary' className='bg-[#90e0ef] text-white flex items-center'><EditOutlined /></Button></NavLink>
      <NavLink key={2} onClick={() => handleDeleteFilm(film)} ><Button type="primary" className='text-white bg-[#fb6f92] flex items-center ml-2'><DeleteOutlined /></Button></NavLink>
      <NavLink key={3} to={`/admin/showtime/${film.maPhim}`} ><Button type="primary" className='text-white bg-[#80ed99] flex items-center ml-2'><PieChartOutlined /></Button></NavLink>
      </div>
    }
  },
];
  return (
    <div>
      <h2 className='text-blue-500 font-bold text-lg'>Quản lý phim</h2>
      <NavLink to="/admin/films/addnew"><Button type='primary' className='mb-3'>Thêm phim</Button></NavLink>
      <Search
      placeholder="input search text"
      enterButton="Search"
      size="large"
      onChange={handleSerachOnChange}
    />
      <Table dataSource={arrFilm} columns={headersFilm} />;
    </div>
  )
}
