import { PlusOutlined } from '@ant-design/icons';
import {
  Button, Cascader, DatePicker, Form, Input, InputNumber, message, Radio, Select, Switch, TreeSelect, Upload,
} from 'antd';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { themPhimUploadHinhAction } from '../../../redux/actions/QuanLyPhimAction';
const AdminAddFilm = () => {
  console.log('re-render')
  const [componentSize, setComponentSize] = useState('default');
  const [imgSrc, setImgSrc] = useState({})
  const [file, setFile] = useState(null)
  const dispatch = useDispatch()
  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };
  const onFinish = (values) => {
    let ngayKhoiChieu = values.ngayKhoiChieu.format('DD/MM/YYYY')
    const newValues = { ...values, maNhom: 'GP11', hinhAnh: file, ngayKhoiChieu: ngayKhoiChieu }
    const formData = new FormData()
    for (let key in newValues) {
      if (key !== 'hinhAnh') {
        formData.append(key, newValues[key])
      } else {
        formData.append('File', newValues.hinhAnh, newValues.hinhAnh.name)
      }
    }
    dispatch(themPhimUploadHinhAction(formData))
  };
  const validateMessages = {
    required: '${label} is required!',
    types: {
      email: '${label} is not a valid email!',
      number: '${label} is not a valid number!',
    },
    number: {
      range: '${label} must be between ${min} and ${max}',
    },
  };

  const handleChangeFile = (e) => {
    // lấy ra file từ e
    let file = e.target.files[0]
    setFile(file)
    // tạo đối tượng để đọc file
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = (e) => {
      setImgSrc(e.target.result)
    }
  }
  return (
    <div>
      <h2 className='text-lg'>Thêm phim mới</h2>
      <Form onFinish={onFinish}
        validateMessages={validateMessages}
        className='containner mx-auto'
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
        style={{
          maxWidth: 600,
        }}
      >
        <Form.Item label="Form Size" name="size" >
          <Radio.Group>
            <Radio.Button value="small">Small</Radio.Button>
            <Radio.Button value="default">Default</Radio.Button>
            <Radio.Button value="large">Large</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Tên phim" name='tenPhim' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label="Trailer" name='trailer' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label="Mô tả" name='moTa' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label="Ngày chiếu" name='ngayKhoiChieu'>
          <DatePicker format={'DD/MM/YY'} />
        </Form.Item>
        <Form.Item label="Đang chiếu" valuePropName="checked" initialValue={false} name='dangChieu'>
          <Switch />
        </Form.Item>
        <Form.Item label="Sắp chiếu" valuePropName="checked" initialValue={false} name='sapChieu'>
          <Switch />
        </Form.Item>
        <Form.Item label="Hot" valuePropName="checked" initialValue={false} name='hot'>
          <Switch />
        </Form.Item>
        <Form.Item label="Số sao" name='soSao' rules={[{ required: true, type: 'number', min: 1, max: 10 }]}>
          <InputNumber />
        </Form.Item>
        <Form.Item label="Thêm hình"
          rules={[{ required: true, message: 'Vui lòng chọn hình ảnh!' }]}>
          <input type="file" onChange={handleChangeFile} />
          <img src={imgSrc} className="w-[120px] h-[120px] rounded-md mt-3" alt="" />
        </Form.Item>
        <Form.Item label="Thêm phim">
          <Button htmlType='submit' type="primary">Thêm</Button>
        </Form.Item>
      </Form>
    </div>
  );
};
export default AdminAddFilm