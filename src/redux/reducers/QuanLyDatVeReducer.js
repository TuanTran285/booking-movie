import { ThongTinPhongVe } from "../../_core/model/ThongTinPhongVe"
import { DAT_VE, DAT_VE_HOAN_TAT, GET_CHI_TIET_PHONG_VE } from "../actions/Type/QuanLyDatVeType"

const initialState = {
    chiTietPhongVe: new ThongTinPhongVe(),
    DanhSachgheDangDat: [],
    danhSachGheKhachDat: [{maGhe: 104521},{maGhe: 104522},{maGhe: 104523},{maGhe: 104524}]
}

const QuanLyDatVeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CHI_TIET_PHONG_VE: {
        state.chiTietPhongVe = action.chiTietPhongVe
        return {...state}
    }

    case DAT_VE: {
      let newDanhSachGheDangDat = [...state.DanhSachgheDangDat]
      let index = newDanhSachGheDangDat.findIndex(ghe => {
        return ghe.maGhe === action.ghe.maGhe
      })
      if(index !== -1) {
        newDanhSachGheDangDat.splice(index, 1)
      }else {
        newDanhSachGheDangDat.push(action.ghe)
      }
      state.DanhSachgheDangDat = newDanhSachGheDangDat
      return {...state}
    }
    case DAT_VE_HOAN_TAT: {
      state.DanhSachgheDangDat = []
      return {...state}
    }
  default:
    return state
  }
}

export default QuanLyDatVeReducer