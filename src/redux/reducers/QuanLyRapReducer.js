import { SET_HE_THONG_RAP, SET_THONG_TIN_PHIM } from "../actions/Type/QuanLyRapType"

const initialValues = {
    heThongRapChieu: [],
    detailFilm: {}
}

const QuanLyRapReducer =  (state = initialValues, action) => {
  switch (action.type) {
    case SET_HE_THONG_RAP: {
        state.heThongRapChieu = action.heThongRapChieu
        return {...state}
    }
    case SET_THONG_TIN_PHIM: {
      state.detailFilm = action.detailFilm
      return {...state}
    }
  default:
    return state
  }
}

export default QuanLyRapReducer