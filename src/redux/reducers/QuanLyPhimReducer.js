import { SET_DANH_SACH_PHIM, SET_THONG_TIN_PHIM } from "../actions/Type/QuanLyPhimType"

const initialState = {
  arrFilm: [],
  thongTinPhim: {}
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_DANH_SACH_PHIM: {
      state.arrFilm = action.arrFilm
      return {...state}
    }
    case SET_THONG_TIN_PHIM: {
      state.thongTinPhim = action.thongTinPhim
      return {...state}
    }
  default:
      return state
  }
}
