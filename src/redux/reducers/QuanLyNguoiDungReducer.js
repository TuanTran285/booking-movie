import { TOKEN, USER_LOGIN } from "../../util/settings/config"
import { SET_DANH_SACH_NGUOI_DUNG, SET_NGUOI_DUNG_DANG_NHAP, SET_THONG_TIN_NGUOI_DUNG, } from "../actions/Type/QuanLyNguoiDungType"


let user = null

if(localStorage.getItem(USER_LOGIN)) {
    user = JSON.parse(localStorage.getItem(USER_LOGIN))
}

const initialState = {
    userLogin: user,
    thongTinNguoiDung: {},
    danhSachNguoiDung: [],
    danhSachTimKiemNguoiDung: []
}

const QuanLyNguoiDungReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_NGUOI_DUNG_DANG_NHAP: {
        state.userLogin = action.userLogin
        localStorage.setItem(USER_LOGIN, JSON.stringify(action.userLogin))
        localStorage.setItem(TOKEN, action.userLogin.accessToken)
        return {...state}
    }
    case SET_THONG_TIN_NGUOI_DUNG: {
      state.thongTinNguoiDung = action.thongTinNguoiDung
      return {...state}
    }
    case SET_DANH_SACH_NGUOI_DUNG: {
      state.danhSachNguoiDung = action.danhSachNguoiDung
      return {...state}
    }

  default:
    return state
  }
}

export default QuanLyNguoiDungReducer
