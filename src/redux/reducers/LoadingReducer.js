import { LOADING_HIDEN, LOADING_PLAY } from "../actions/Type/LoadingType"

const initialState = {
    isLoading: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LOADING_PLAY: {
        state.isLoading = true
        return {...state}
    }
    case LOADING_HIDEN: {
        state.isLoading = false
        return {...state}
    }

  default:
    return state
  }
}
