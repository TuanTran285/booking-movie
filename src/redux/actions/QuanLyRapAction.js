import axios from "axios"
import { quanLyRapService } from "../../services/QuanLyRapService"
import { SET_HE_THONG_RAP, SET_THONG_TIN_PHIM } from "./Type/QuanLyRapType"
// bọc 1 hàm trong 1 hàm có chức năng closure lấy thâm số có function cha sử dụng cho function con
export const layThongTinHeThongRapAction = () => {
    return async (dispatch) => {
        try {
            const result = await quanLyRapService.layHeThongRap()
            if(result.status === 200) {
                dispatch({
                    type: SET_HE_THONG_RAP,
                    heThongRapChieu: result.data.content
                })
            }
        }
        catch (err) {
            console.log(123)
        }
    }
}


export const layChiTietThongTinPhimAction = (id) => {
    return async (dispatch) => {
        try {
            const result = await quanLyRapService.layChiTietThongTinPhim(id)
            dispatch({
                type: SET_THONG_TIN_PHIM,
                detailFilm: result.data.content
            })
        }
        catch(err) {
            console.log("err", err)
        }
    }
}