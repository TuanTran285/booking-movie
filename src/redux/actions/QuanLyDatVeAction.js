import { quanLyDatVeService } from "../../services/QuanLyDatVeService"
import { DAT_VE_HOAN_TAT, GET_CHI_TIET_PHONG_VE } from "./Type/QuanLyDatVeType"
import { toast } from 'react-toastify'
import { loadingHidenAction } from "./LoadingAction"
import { layThongTinNguoiDungAction } from "./QuanLyNguoiDungAction"

export const layThongTinPhongVeAction = (maLichChieu) => {
    return async (dispatch) => {
        try {
            const result = await quanLyDatVeService.layChiTietPhongVe(maLichChieu)
            if(result.status === 200) {
                dispatch({
                    type: GET_CHI_TIET_PHONG_VE,
                    chiTietPhongVe: result.data.content
                })
            }
        }
        catch (err) {
           console.log("err", err)
        }
    }
}


export const datVeAction = (thongTinDatVe, handleChangeTab) => {
    const notifySuccess = () => toast.success("Đặt vé thành công :))")
    const notifyError = () => toast.error("Vui lòng chọn ghế :((")
    console.log(thongTinDatVe)
    return async (dispatch) => {
        try {
            const result = await quanLyDatVeService.datVeService(thongTinDatVe)
            if(result.status === 200) {
                if(thongTinDatVe.danhSachVe.length === 0) {
                    notifyError()
                    setTimeout(() => {
                        dispatch(loadingHidenAction)
                    }, 500)
                }else {
                    // đặt vé thành công gọi api load lại phòng vé
                    await dispatch(layThongTinPhongVeAction(thongTinDatVe.maLichChieu))
                    await handleChangeTab()
                    await dispatch(layThongTinNguoiDungAction())
                    await dispatch({type: DAT_VE_HOAN_TAT})
                    await dispatch(loadingHidenAction)
                    notifySuccess()
                }
            }
        }
        catch (err) {
            dispatch(loadingHidenAction)
           console.log("err", err)
        }
    }
}
