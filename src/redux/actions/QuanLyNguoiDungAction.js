import { message } from "antd"
import { quanLyNguoiDungService } from "../../services/QuanLyNguoiDungService"
import { SET_DANH_SACH_NGUOI_DUNG, SET_NGUOI_DUNG_DANG_NHAP, SET_THONG_TIN_NGUOI_DUNG, SET_TIM_NGUOI_DUNG } from "./Type/QuanLyNguoiDungType"
import { USER_LOGIN } from "../../util/settings/config"

export const layThongTinDangNhapAction = (thongTinDangNhap, onSuccess) => {
    return async (dispatch) => {
        try {
            const result = await quanLyNguoiDungService.dangNhap(thongTinDangNhap)
            if(result.status === 200) {
                dispatch({
                    type: SET_NGUOI_DUNG_DANG_NHAP,
                    userLogin: result.data.content
                })
                onSuccess()
            }
        }
        catch (err) {
           console.log("err", err)
        }
    }
}

export const layThongTinNguoiDungAction = () => {
    return async (dispatch) => {
        try {
            const result = await quanLyNguoiDungService.layThongTinNguoiDung()
            if(result.status === 200) {
                dispatch({
                    type: SET_THONG_TIN_NGUOI_DUNG,
                    thongTinNguoiDung: result.data.content
                })
            }
        }
        catch (err) {
           console.log("err", err)
        }
    }
}

export const layDanhSachNguoiDungAction = (tuKhoa) => {
    return async (dispatch) => {
        try{
            const result = await quanLyNguoiDungService.layDanhSachNguoiDung(tuKhoa)
            if(result.status === 200) {
                dispatch({
                    type: SET_DANH_SACH_NGUOI_DUNG,
                    danhSachNguoiDung: result.data.content
                })
            }
        }catch(err) {
            console.log("err", err)
        }
    }
}

export const xoaNguoiDungAction = (taiKhoan) => {
    return async (dispatch) => {
        try{
            const result = await quanLyNguoiDungService.xoaNguoiDung(taiKhoan)
            if(result.status === 200) {
                message.success("Xóa người dùng thành công")
            }
        }catch(err) {
            // console.log("err", err.response.data.content)
            message.error(err.response?.data.content)
        }
    }
}