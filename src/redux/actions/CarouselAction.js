import axios from "axios"
import Loading from "../../components/Loading/Loading"
import { quanLyPhimService } from "../../services/QuanLyPhimService"
import { DOMAIN } from "../../util/settings/config"
import { loadingHidenAction } from "./LoadingAction"
import { SET_CAROUSEL } from "./Type/CarouselType"
// bọc 1 hàm trong 1 hàm có chức năng closure lấy thâm số có function cha sử dụng cho function con
export const getCarouselAction = () => {
    return async (dispatch) => {
        try {
            const result = await quanLyPhimService.layDanhSachBanner()
            if (result.status === 200) {
                dispatch({
                    type: SET_CAROUSEL,
                    arrImg: result.data.content
                })
            }
        }
        catch (err) {
            console.log(123)
        }
    }
}