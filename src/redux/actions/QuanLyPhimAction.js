import { message } from "antd"
import { quanLyPhimService } from "../../services/QuanLyPhimService"
import { SET_DANH_SACH_PHIM, SET_THONG_TIN_PHIM } from "./Type/QuanLyPhimType"
// bọc 1 hàm trong 1 hàm có chức năng closure lấy thâm số có function cha sử dụng cho function con
export const layDanhSachPhimAction = (tuKhoa) => {
    return async (dispatch) => {
        try {
            const result = await quanLyPhimService.layDanhSachPhim(tuKhoa)
            dispatch({
                type: SET_DANH_SACH_PHIM,
                arrFilm: result.data.content
            })
        }
        catch (err) {
            console.log('err', err)
        }
    }
}
export const themPhimUploadHinhAction = (formData) => {
    return async (dispatch) => {
        try {
            const result = await quanLyPhimService.themPhimUploadHinh(formData)
            message.success("Thêm phim hoàn tất")
        }
        catch (err) {
            console.log('err', err.response?.data)
        }
    }
}

export const layThongTinPhimAction = (maPhim) => {
    return async dispatch => {
        try {
            const result = await quanLyPhimService.layThongTinPhim(maPhim)
            if (result.status === 200) {
                dispatch({
                    type: SET_THONG_TIN_PHIM,
                    thongTinPhim: result.data.content
                })
            }
        } catch (err) {
            console.log('err', err.response?.data)
        }
    }
}

export const capNhatPhimUploadAction = (formData) => {
    return async dispatch => {
        try {
            const result = await quanLyPhimService.capNhatPhimUpload(formData)
            message.success("Cập nhật phim thành công")
        } catch (err) {
            console.log('err', err)
        }
    }
}

export const xoaPhimAction = (maPhim) => {
    return async dispatch => {
        try {
            const result = await quanLyPhimService.xoaPhim(maPhim)
            message.success("Xóa phim thành công")
        } catch (err) {
            console.log('err', err)
            message.error(err.response.data.content)
        }
    }
}