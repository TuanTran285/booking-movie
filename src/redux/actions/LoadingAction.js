import { LOADING_HIDEN, LOADING_PLAY } from "./Type/LoadingType";

export const loadingPlayAction = {
    type: LOADING_PLAY
}

export const loadingHidenAction = {
    type: LOADING_HIDEN
}