import React from 'react'
import { NavLink, useParams } from 'react-router-dom'

export default function Film(props) {
  let {phim} = props
  return (
    <div className="bg-gray-100 mx-8 mb-12 bg-opacity-75 shadow rounded-lg overflow-hidden relative" style={{ height: 'auto' }}>
      <div className='rounded-lg bg-no-repeat' style={{ backgroundImage: `url(${phim.hinhAnh})`, backgroundPosition: 'center', backgroundSize: 'cover' }}>
        <img src={phim.hinhAnh} className='w-full opacity-0' style={{ height: 300 }} alt="Lỗi" />
      </div>
      <div className='mt-4 h-28'>
        <h3 className='font-semibold text-lg'>{phim.tenPhim}</h3>
        <div>{phim.moTa.length > 50 ? phim.moTa.slice(0, 50) + '...' : phim.moTa}</div>
      </div>
      <NavLink to={`/detail/${phim.maPhim}`}><button className='p-3 block w-full text-white bg-cyan-500'>Mua vé</button></NavLink>
    </div>
  )
}
