import Search from 'antd/es/input/Search';
import React from 'react'
import './InputSearch.css'
export default function InputSearch() {
const onSearch = (value) => console.log(value);
  return (
    <div>
        <Search
        type='primary'
        placeholder="input search text"
        enterButton="search"
        size="large"
        onChange={onSearch}
      />
    </div>
  )
}
