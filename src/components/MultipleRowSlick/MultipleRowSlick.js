import React, { Component } from "react";
import { useParams } from "react-router-dom";
import Slider from "react-slick";
import Film from "../Film/Film";
import styleSlick from './MultipleCustomArrow.css'
function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={`${className}`}
        style={{ ...style, display: "block"}}
        onClick={onClick}
      />
    );
  }
  
  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={`${className}`}
        style={{ ...style, display:'block'}}
        onClick={onClick}
      />
    );
  }
export default class MultipleRowSlick extends Component {

    renderArrFilm = () => {
        return this.props.arrFilm.map((phim, index) => {
            return <div className="setWidth" key={index}>
                <Film phim={phim} />
            </div>
        })
    }
    render() {
        const settings = {
            className: "slider variable-width",
            dots: true,
            infinite: false,
            centerPadding: "60px",
            initialSlide: 0,
            slidesToShow: 4,
            slidesToScroll: 4,
            speed: 500,
            rows: 1,
            slidesPerRow: 2,
            variableWidth: true,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <div>
                <Slider {...settings}>
                    {this.renderArrFilm()}
                </Slider>

            </div>
        )
    }
}