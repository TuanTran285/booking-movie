import React from 'react'
import { Fragment } from 'react'
import { useSelector } from 'react-redux'
export default function Loading() {
    const {isLoading} = useSelector(state => state.LoadingReducer)
    {/* dùng loading.gif */ }
    return (
        <Fragment>
            {isLoading? 
            <div className='fixed top-0 left-0 right-0 bottom-0 z-20 overflow-hidden
         flex items-center justify-center bg-[#000000] opacity-[0.7]'>
                <div className=''>
                    <img className='w-28 h-28 object-cover' src={require("../../assets/img/loading/loading6.gif")} alt="loading image" />
                </div>
            </div> : ''}
        </Fragment>
    )
}
